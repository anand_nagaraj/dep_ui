'use strict';
import { stringify as formatQueryString } from 'querystring'
import $ from 'jquery';

export class RestQueries 
{
    constructor() {
        this.query_obj = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "analyze_wildcard": 'true',
                                "query": "*"
                            }
                        },
                        {
                            "range": {
                                "@timestamp": {
                                    "format": "epoch_millis"
                                }
                            }
                        }
                    ],
                    "must_not": []
                }
            },
            "aggs": {
                "2": {
                    "date_histogram": {
                        "field": "@timestamp",
                        "time_zone": "Australia/Sydney",
                        "min_doc_count": 1
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "version": 'true'
        };
    }

    toString() {
        return JSON.stringify(this.query_obj);
    }
    
    print() {
        console.log(this.query_obj);
    }

    update_query(inputDataObject) {
        var fields = inputDataObject.fields;
        var all_agg = {};
        var range = {};
        var ts = {};
        var must = {};
        fields.forEach(function(field) {
            all_agg[field] = {"max":{"field" : field}};
        });

        ts['format'] = 'epoch_millis';
        ts['gte'] = inputDataObject.toEpoch(inputDataObject.start_time);
        ts['lte'] = inputDataObject.toEpoch(inputDataObject.end_time);
        range['@timestamp'] = ts;
        must['range'] = range;

        // Filling other details from input_data
        this.query_obj['aggs']['2']['date_histogram']['interval'] = inputDataObject.granularity;
        this.query_obj['query']['bool']['must'][1] = must;
        if (Object.keys(all_agg).length > 0) {            
            this.query_obj['aggs']['2']['aggs'] = all_agg;
        }

        console.log("Printing all the updated values now");
        this.print();
    }

    sendDEPRequestToES(es_method, es_path, es_data)
    {
        var response = null;
        try {
            response = this.send_request(es_method, es_path, es_data);
        }
        catch(err) {
            console.log("Exception occured." + err);
        }
        finally {
            return response;
        }
    }

    send_request(method, path, data) {
        var response = null;
        console.log("Calling " + path);
        if (data && method == "GET") {
            method = "POST";
        }
    
        let contentType;
    
        if (data) {
            try {
                JSON.parse(data);
                contentType = 'application/json';
            }
            catch (e) {
                try {
                    data.split('\n').forEach(line => {
                        if (!line) return;
                        JSON.parse(line);
                    });
                    contentType = 'application/x-ndjson';
                } catch (e){
                    contentType = 'text/plain';
                }
            }
        }
    
        var options = {
            url: '../api/console/proxy?' + formatQueryString({ path, method }),
            data,
            contentType,
            cache: false,
            crossDomain: true,
            async: false,
            type: 'POST',
            dataType: 'text', // disable automatic guessing
        };
    
        $.ajax(options)
        .done(function(resp) {
            console.log("success");
            response = resp;
            console.log(resp);
        })
        .fail(function() {
            console.log("error");
            response = null;
        })
        .always(function() {
            console.log("complete");
        });
    
        return response;
    }
}
