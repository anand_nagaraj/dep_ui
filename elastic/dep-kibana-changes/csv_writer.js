'use strict';

import { saveAs } from '@spalger/filesaver';

export class CSVWriter {
    constructor(title) {
        this.fileName = title;
        this.finalResults = {};
    }
    print() {
        console.log("CSVWriter: I dont have anything to print for now");
    }

    evaluate_results(results, index, fields) {
        if (results == null || index == null) {
            console.log("Nothing came in results yet.");
            return;
        }
        console.log(results);

        var total = results['hits']['total']
        if (total == 0) {
            return;
        }
        console.log("Data was available in index " + index);

        var prev_val = {};
        var cur_val = {};
        var delta = {};

        /*
        'buckets': [
            {
                'dep_lp.distr.passed_bytes': {
                'value': 38854756398.0
                },
                'dep_lp.distr.incoming_bytes': {
                'value': 38856372475.0
                },
                'doc_count': 764,
                'dep_lp.distr.gtpu_bytes': {
                'value': 38821183265.0
                },
                'key': 1502991000000,
                'dep_lp.distr.failed_bytes': {
                'value': 1064810.0
                },
                'key_as_string': '2017-08-18T03:30:00.000+10:00'
            },
            {
                'dep_lp.distr.passed_bytes': {
                'value': 113853366633.0
                },
                'dep_lp.distr.incoming_bytes': {
                'value': 113858309929.0
                },
                'doc_count': 1412,
                'dep_lp.distr.gtpu_bytes': {
                'value': 113763119021.0
                },
                'key': 1502991600000,
                'dep_lp.distr.failed_bytes': {
                'value': 3317455.0
                },
                'key_as_string': '2017-08-18T03:40:00.000+10:00'
            }
        ]
        */
        if (results['aggregations'] == undefined ||
             results['aggregations']['2'] == undefined ||
             results['aggregations']['2']['buckets'] == undefined) {
            return;
        }

        var buckets = results['aggregations']['2']['buckets'];
        for (var i = 0, len = buckets.length; i < len; ++i) {
            var element = buckets[i];
            var key = element['key'];
            var key_as_string = element['key_as_string'];

            for (var j = 0; j < fields.length; ++j) {
                var field = fields[j];
                cur_val[field] = element[field]['value'];

                // A special case for handling throughput graph.
                if (field.search('throughput') > 0) {
                    delta[field] = cur_val[field] ? cur_val[field] : 0;
                    continue;
                }

                delta[field] = 0; // Should this be first value or 0; not sure.
                if (cur_val[field]) {
                    if (prev_val[field] != undefined) {
                        delta[field] = cur_val[field] - prev_val[field];
                    }
                }
                prev_val[field] = cur_val[field];
            }

            // console.log(key + " : " + key_as_string + " : " + JSON.stringify(delta));
            // this.finalResults[key] = JSON.stringify(delta); // Either this one or the next
            this.finalResults[key_as_string] = JSON.stringify(delta);
        }

    }

    isEmpty() {
        return Object.keys(this.finalResults).length === 0;
    }
    print_results() {
        if (this.isEmpty()) {
            // evaluate_results();
        }
        for(var key in this.finalResults) {
            if (this.finalResults.hasOwnProperty(key)) {
                console.log(key, this.finalResults[key]);
            }
        }
    }

    dump_results_to_csv_file(granularity, from, to) {
        if (this.isEmpty()) {
            // evaluate_results();
            alert("Was unable to get any valid data from the query");
            return;
        }

        var columns = [];
        var fields_data = {};
        var values = [];
        for (var key in this.finalResults) {
            if (this.finalResults.hasOwnProperty(key)) {
                var curr_row_values = this.finalResults[key].split(',');
                for (var i = 0; i < curr_row_values.length; ++i) {
                    var field_value = curr_row_values[i].split(':');
                    var elem = field_value[0];
                    values.push(field_value[1])
                    if (columns.indexOf(elem) < 0 ) {
                        columns.push(elem);
                    }
                }
                fields_data[key] = values;
                values = [];
            }
        }

        var stored_data = ""; // Not sure what's the complexity here to store huge data.
        for (var i in columns) {
            stored_data += "," + columns[i];
        }

        stored_data += "\n";

        // Sort the map based on key i.e timestamp.
        var ordered_fields_data = {};
        Object.keys(fields_data).sort().forEach(function(key) {
            ordered_fields_data[key] = fields_data[key]
        });

        for(var key in ordered_fields_data) {
            stored_data += key;
            var i = 0;
            if (this.finalResults.hasOwnProperty(key)) {
                // console.log(key, this.finalResults[key]);
                var values = ordered_fields_data[key];
                for (var i = 0; i < values.length; ++i) {
                    stored_data += "," + values[i];
                }

                stored_data += "\n";
            }
        }
        console.log(stored_data);

        if (stored_data.length > 0) {
            console.log(stored_data);
            const blob = new Blob([stored_data], {type: "text/plain;charset=utf-8"});
            if (this.fileName) {
                saveAs(blob, this.fileName + "_from_" + from + "_to_" + to + '.csv');
            } else {
                saveAs(blob, granularity + "_from_" + from + "_to_" + to + '.csv');
            }
        }
    }
}
