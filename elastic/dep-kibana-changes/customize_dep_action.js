'use strict';

import { CSVWriter } from './csv_writer';
import { RestQueries } from './rest_queries';

export class InputData {
    constructor(dur, fd, stime, etime) {
        this.granularity = dur;
        this.fields = fd;
        this.start_time = stime;
        this.end_time = etime;
    }
    print(){
        console.log('granularity:' + this.granularity +
            ' fields:' + this.fields +
            ' epoch from:' + this.start_time +
            ' epoch to:' + this.end_time);
    }
    toEpoch(fromString) {
        // FIXME:
        // We are not doing any validation here, it may throw an exception if this is plain numeric or not in expected string format
        // Also, we need to convert from GMT to localtime.
        return new Date(fromString).getTime();
    }
}

export class CustomizeDEPAction {
    // This is the main handler which takes complete parameters in hand
    constructor(granularity, fields, from, to, title) {
        this.inputdata = new InputData(granularity, fields, from, to);
        this.restQueries = new RestQueries();
        this.csvWriter = new CSVWriter(title);
    }

    print() {
        this.inputdata.print();
        this.csvWriter.print();
        this.restQueries.print();
    }

    update_restquery() {
        console.log("Updating the rest query");
        this.restQueries.update_query(this.inputdata);
    }
    indices_query() {
        var indices = this.restQueries.sendDEPRequestToES("GET", "_cat/indices/logstash-*/?v&h=i", null);
        console.log('Final output' + indices);
        if (indices) {
            indices.split('\n').forEach(function(item, index, array) {
                console.log(item, index);
            });
        } else {
            console.log("No indices found here.");
            return null;
        }
        return indices;
    }

    collapseLiteralStrings = function (data) {
        return data.replace(/"""(?:\s*\n)?((?:.|\n)*?)(?:\n\s*)?"""/g,function (match, literal) {
            return JSON.stringify(literal);
        });
    }

    search_and_print(indices) {
        if (indices) {
            var ind_arr = indices.split('\n');
            for(let i = 1; i < ind_arr.length; ++i) {
                var item = ind_arr[i];
                if (item == "") continue;
                console.log(item, i);

                var uri_search = item + '/_search';
                var data = this.collapseLiteralStrings(this.restQueries.toString());
                if (data) {
                    data += "\n";
                }
                var res = this.restQueries.sendDEPRequestToES("GET", uri_search, data);
                if (res == null) {
                    continue;
                }
                var jsonObjectResult = JSON.parse(res);

                this.csvWriter.evaluate_results(jsonObjectResult, item, this.inputdata.fields);
            }
            this.csvWriter.print_results();
            this.csvWriter.dump_results_to_csv_file(this.inputdata.granularity,
                                                    this.inputdata.start_time,
                                                    this.inputdata.end_time);
        }
    }
}
