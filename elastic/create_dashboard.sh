#!/bin/bash

# Usage examples:
# env KIBANA_INDEX='.kibana_env1' ./load.sh
# ./load.sh -url http://test.com:9200
# ./load.sh -url http://test.com:9200 -user admin:secret
# ./load.sh -url http://test.com:9200 -index .kibana-test

# The default value of the variable. Initialize your own variables here
ELASTICSEARCH=http://localhost:9200
CURL=curl
KIBANA_INDEX=".kibana"
BEAT_CONFIG=".beatconfig"

MODULE_ARRAY=('dep_lp.distr' 'dep_cp')

DURATION_ARRAY=('1m' '10m' '30m' '1h' '6h' '12h' '1d' '2d' '1w' '4w')
DISTR_ARRAY=('incoming' 'passed' 'failed' 'frag' 'defrag' 'gtpu' 'failed_gtpu' 'gtpc' 'failed_gtpc' 's1mme' 'failed_s1mme') # 'iups')
DISTR_TYPE_ARRAY=('pkts' 'bytes' 'pkts_throughput' 'bytes_throughput') # 'pkts_percentage' 'bytes_percentage')
MYRICOM_ARRAY=('recv_pkts' 'recv_bytes')
ETHERNET_ARRAY=('recv_pkts_since_dep_started' 'recv_bytes_since_dep_started')
UPLANE_ARRAY=('duplicate_pkts' 'duplicate_bytes' 'failed_s1_pkts' 'failed_s2_pkts' 'sent_pkts' 'sent_bytes')
CPLANE_ARRAY=('total_pkts' 'duplicate_pkts')

STAGE_ARRAY_CP=('report' 'proc' 'recv')
TYPE_ARRAY_CP=('bytes' 'ul' 'dl' 'link' 'unlink' 'per')

declare -A E2E_ARRAY
E2E_ARRAY=([lp_myricom_recv_bytes]=dep_lp.src.myricom.recv_bytes
           [lp_gtpu_bytes]=dep_lp.distr.gtpu_bytes
           [lp_uplane_sent_bytes]=dep_lp.uplane.sent_bytes
           [cp_report_unlink]=dep_cp.report_unlink
           )

declare -A FAIL_ARRAY
FAIL_ARRAY=([lp_failed_bytes]=dep_lp.distr.failed_bytes
            [lp_failed_gtpu_bytes]=dep_lp.distr.failed_gtpu_bytes
            [lp_failed_gtpu_bytes]=dep_lp.distr.failed_gtpc_bytes
            [lp_failed_gtpc_bytes]=dep_lp.distr.failed_gtpc_bytes
            [lp_failed_s1mme_bytes]=dep_lp.distr.failed_s1mme_bytes
            [lp_uplane_duplicate_bytes]=dep_lp.uplane.duplicate_bytes
            [lp_cplane_duplicate_pkts]=dep_lp.cplane.duplicate_pkts
            [lp_uplane_failed_s1_pkts]=dep_lp.uplane.failed_s1_pkts
            [lp_uplane_failed_s2_pkts]=dep_lp.uplane.failed_s2_pkts
            )

VISUALIZATION_PATH='dashboards/visualization'
DASHBOARD_PATH='dashboards/dashboard'
INDEX_PATTERN_PATH='dashboards/index_pattern'

STATIC_CONST="\\\\"\"size_x\\\\"\":6, \\\\"\"size_y\\\\"\":3, \\\\"\"type\\\\"\":\\\\"\"visualization\\\\"\""


print_usage()
{
  echo "

Load the dashboards, visualizations and index patterns into the given
Elasticsearch instance.

Usage:
  $(basename "$0") -url ${ELASTICSEARCH} -user admin:secret -index ${KIBANA_INDEX} -gen_visualization -gen_dashboard

Options:
  -h | -help
    Print the help menu.
  -l | -url
    Elasticseacrh URL. By default is ${ELASTICSEARCH}.
  -u | -user
    Username and password for authenticating to Elasticsearch using Basic
    Authentication. The username and password should be separated by a
    colon (i.e. "admin:secret"). By default no username and password are
    used.
  -i | -index
    Kibana index pattern where to save the dashboards, visualizations,
    index patterns. By default is ${KIBANA_INDEX}.
  -gv | -gen_visualization
     Generate visualization granularity and exit
  -gd | -gen_dashboard
     Generate dashboard granularity and exit

" >&2
}


generate_myri_ether_cplane_uplane_visualization_granularity_process()
{
    ARGS=$1
    echo "Generating ${ARGS} visualization for granularity"

    if [ ${ARGS} == "myricom" ]; then
        STAGE_ARRAY=${MYRICOM_ARRAY[@]}
        PREFIX='dep_lp.src'
    elif [ ${ARGS} == "ethernet" ]; then
        STAGE_ARRAY=${ETHERNET_ARRAY[@]}
        PREFIX='dep_lp.src'
    elif [ ${ARGS} == "uplane" ]; then
        STAGE_ARRAY=${UPLANE_ARRAY[@]}
        PREFIX='dep_lp'
    elif [ ${ARGS} == "cplane" ]; then
        STAGE_ARRAY=${CPLANE_ARRAY[@]}
        PREFIX='dep_lp'
    fi

    for DURATION in ${DURATION_ARRAY[@]}
    do
        for STAGE in ${STAGE_ARRAY[@]}
        do
            DATA=${ARGS}_${STAGE}
            OBJECT=${PREFIX}.${ARGS}.${STAGE}
            TITLE=${DURATION}_'lp'_${DATA}
            VISUALIZATION_FILE=${VISUALIZATION_PATH}/${TITLE}.json

            SUBTRACT=".subtract(.es(metric='max:${OBJECT}', offset=-${DURATION}))"
            FORMAT="format()"

            if [[ ${TYPE} =~ .*throughput ]]; then
                FORMAT="format_bps()"
                SUBTRACT=
            fi

read -d '' output <<- EOF
{
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"query\\\":{\\\"query_string\\\":{\\\"query\\\":\\\"*\\\"}},\\\"filter\\\":[]}"
    },
    "version": 1,
    "description": "",
    "uiStateJSON": "{}",
    "visState": "{\\\"title\\\":\\\"${TITLE}\\\",\\\"type\\\":\\\"timelion\\\",\\\"params\\\":{\\\"expression\\\":\\\".es(metric='max:${OBJECT}')${SUBTRACT}.label(${DATA}).${FORMAT}\\\",\\\"interval\\\":\\\"${DURATION}\\\",\\\"fields\\\":\\\"${OBJECT}\\\"},\\\"aggs\\\":[],\\\"listeners\\\":{}}",
    "title": "${TITLE}"
}
EOF
             echo "Generated $VISUALIZATION_FILE"
             echo "${output}" > ${VISUALIZATION_FILE}

        done # For all Stage Array
    done #For all Duration
}


generate_visualization_granularity_process()
{
    ARGS=$1
    echo "Generating visualization for granularity for ${ARGS}"
    TYPE_ARRAY=()
    STAGE_ARRAY=()
    MODULE=${MODULE_ARRAY[0]}

    if [ ${ARGS} == "lp" ]; then
        TYPE_ARRAY=${DISTR_TYPE_ARRAY[@]}
        STAGE_ARRAY=${DISTR_ARRAY[@]}
    else
        TYPE_ARRAY=${TYPE_ARRAY_CP[@]}
        STAGE_ARRAY=${STAGE_ARRAY_CP[@]}
        MODULE=${MODULE_ARRAY[1]}
    fi

    for DURATION in ${DURATION_ARRAY[@]}
    do
        for STAGE in ${STAGE_ARRAY[@]}
        do
            for TYPE in ${TYPE_ARRAY[@]}
            do
                if [ ${ARGS} == "cp" ]; then
                if [ ${STAGE} == "report" -o ${STAGE} == "recv" ]; then
                if [ ${TYPE} == "ul" -o ${TYPE} == "dl" ]; then
                    continue
                fi
                fi
                fi

                DATA=${STAGE}_${TYPE}
                OBJECT=$MODULE.$DATA
                TITLE=${DURATION}_${ARGS}_${DATA}
                VISUALIZATION_FILE=${VISUALIZATION_PATH}/${TITLE}.json

                SUBTRACT=".subtract(.es(metric='max:${OBJECT}', offset=-${DURATION}))"
                FORMAT="format()"

                if [[ ${TYPE} =~ .*throughput ]]; then
                    FORMAT="format_bps()"
                    SUBTRACT=
                fi

read -d '' output <<- EOF
{
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"query\\\":{\\\"query_string\\\":{\\\"query\\\":\\\"*\\\"}},\\\"filter\\\":[]}"
    },
    "version": 1,
    "description": "",
    "uiStateJSON": "{}",
    "visState": "{\\\"title\\\":\\\"${TITLE}\\\",\\\"type\\\":\\\"timelion\\\",\\\"params\\\":{\\\"expression\\\":\\\".es(metric='max:${OBJECT}')${SUBTRACT}.label(${DATA}).${FORMAT}\\\",\\\"interval\\\":\\\"${DURATION}\\\",\\\"fields\\\":\\\"${OBJECT}\\\"},\\\"aggs\\\":[],\\\"listeners\\\":{}}",
    "title": "${TITLE}"
}
EOF
                echo "Generated $VISUALIZATION_FILE"
                echo "${output}" > ${VISUALIZATION_FILE}

            done # For all Type Array
        done # For all Stage Array
    done #For all Duration
}


generate_dashboard_granularity_process()
{
    ARGS=$1
    TYPE_ARRAY=()
    STAGE_ARRAY=()

    echo "Generating dashboard for granularity ${ARGS}"

    if [ ${ARGS} == "lp" ]; then
        TYPE_ARRAY=${DISTR_TYPE_ARRAY[@]}
        STAGE_ARRAY=${DISTR_ARRAY[@]}
        MODULE=${MODULE_ARRAY[0]}
    else
        TYPE_ARRAY=${TYPE_ARRAY_CP[@]}
        STAGE_ARRAY=${STAGE_ARRAY_CP[@]}
        MODULE=${MODULE_ARRAY[1]}
    fi


    PANEL=''
    PANELINDEX=1
    COL=7
    ROW=0

    #set -x

    for DURATION in ${DURATION_ARRAY[@]}
    do
    PANEL=''
    ROW=0
    COL=7
    PANELINDEX=1
    for STAGE in ${STAGE_ARRAY[@]}
    do
        # Column should be swapped b/w 1 & 7
        if [ $ROW == 0 ]; then
            ROW=1
        else
            ROW=$(expr $ROW + 3) # Always increment it by 3
        fi

        for TYPE in ${TYPE_ARRAY[@]}
        do
            # Column should be swapped b/w 1 & 7
            if [ $COL == 7 ]; then
                COL=1
            elif [ $COL == 1 ]; then
                COL=7
            fi

            CUR_OBJECT=$MODULE.${STAGE}_${TYPE}
            if [ ${ARGS} == "cp" ]; then
                if [ ${STAGE} == "report" -o ${STAGE} == "recv" ]; then
                    if [ ${TYPE} == "ul" -o ${TYPE} == "dl" ]; then
                        continue
                    fi
                fi
            fi

            DATA=${STAGE}_${TYPE}
            IDNAME=${DURATION}_${ARGS}_${DATA}

LOC="{\\\\"\"id\\\\"\":\\\\"\"${IDNAME}\\\\"\", \\\\"\"row\\\\"\":${ROW}, \\\\"\"col\\\\"\":${COL}, \\\\"\"panelIndex\\\\"\":${PANELINDEX}, ${STATIC_CONST}, \\\\"\"field\\\\"\":\\\\"\"${CUR_OBJECT}\\\\"\"}"

            if [[ -z $PANEL ]]; then
                PANEL="${LOC}"           # This is the first one
            else
                PANEL="${PANEL},${LOC}"  # This is NOT the first one
            fi

            PANELINDEX=$(expr $PANELINDEX + 1)
        done

    done

    TITLE="${DURATION}_${ARGS}_dashboard"
    DASHBOARD_FILE=${DASHBOARD_PATH}/${TITLE}.json

read -d '' OUTPUT <<- EOF
{
    "title": "${TITLE}",
    "hits": 0,
    "description": "",
    "optionsJSON": "{\\\"darkTheme\\\":false}",
    "uiStateJSON": "{}",
    "version": 1,
    "timeRestore": false,
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"filter\\\":[{\\\"query\\\":{\\\"query_string\\\":{\\\"analyze_wildcard\\\":true,\\\"query\\\":\\\"*\\\"}}}],\\\"highlightAll\\\":true,\\\"version\\\":true}"
    },
    "panelsJSON": "[${PANEL}]"
}

EOF

    echo "$OUTPUT" > ${DASHBOARD_FILE}
    echo "Generated ${DASHBOARD_FILE}"

    done  # End of outer loop Granularity
}


generate_visualization_granularity_mixed()
{
    ARGS=$1
    echo "Generating visualization for mixed chart granular level for ${ARGS}"
    TYPE_ARRAY=()
    STAGE_ARRAY=()
    MODULE=${MODULE_ARRAY[0]}

    if [ ${ARGS} == "lp" ]; then
        TYPE_ARRAY=${DISTR_TYPE_ARRAY[@]}
        STAGE_ARRAY=${DISTR_ARRAY[@]}
    else
        TYPE_ARRAY=${TYPE_ARRAY_CP[@]}
        STAGE_ARRAY=${STAGE_ARRAY_CP[@]}
        MODULE=${MODULE_ARRAY[1]}
    fi

    for DURATION in ${DURATION_ARRAY[@]}
    do
        for TYPE in ${TYPE_ARRAY[@]}
        do
            QUERY=
            CUR_OBJECT=()
            for STAGE in ${STAGE_ARRAY[@]}
            do
                if [ ${ARGS} == "cp" ]; then
                    if [ ${STAGE} == "report" -o ${STAGE} == "recv" ]; then
                        if [ ${TYPE} == "ul" -o ${TYPE} == "dl" ]; then
                            continue
                        fi
                    fi
                fi

                DATA=${STAGE}_${TYPE}
                OBJECT=$MODULE.$DATA

                if [[ -z $CUR_OBJECT ]]; then
                    CUR_OBJECT=${OBJECT}
                else
                    CUR_OBJECT=${CUR_OBJECT},${OBJECT}
                fi

                SUBTRACT=".subtract(.es(metric='max:${OBJECT}', offset=-${DURATION}))"
                FORMAT="format()"

                if [[ ${TYPE} =~ .*throughput ]]; then
                    FORMAT="format_bps()"
                    SUBTRACT=
                fi

                QUERY=${QUERY}".es(metric='max:${OBJECT}')${SUBTRACT}.label(${DATA}),"
            done # For all Type Array

            TITLE="${DURATION}_mixed_${ARGS}_${TYPE}"
            VISUALIZATION_FILE=${VISUALIZATION_PATH}/${TITLE}.json

read -d '' output <<- EOF
{
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"query\\\":{\\\"query_string\\\":{\\\"query\\\":\\\"*\\\"}},\\\"filter\\\":[]}"
    },
    "version": 1,
    "description": "",
    "uiStateJSON": "{}",
    "visState": "{\\\"title\\\":\\\"${TITLE}\\\",\\\"type\\\":\\\"timelion\\\",\\\"params\\\":{\\\"expression\\\":\\\"${QUERY}\\\",\\\"interval\\\":\\\"${DURATION}\\\",\\\"fields\\\":\\\"${CUR_OBJECT}\\\"},\\\"aggs\\\":[],\\\"listeners\\\":{}}",
    "title": "${TITLE}"
}
EOF
             echo "Generated $VISUALIZATION_FILE"
             echo "${output}" > ${VISUALIZATION_FILE}

        done # For all type Array
    done #For all Duration
}


generate_custom_visualization_granularity_mixed()
{
    ARGS=$1
    echo "Generating custom visualization for mixed chart granular level for ${ARGS}"
    TYPE_ARRAY=()
    STAGE_ARRAY=()
    FIELDS=

    if [ ${ARGS} == "e2e" ]; then
        typeset -A ITEM_ARRAY
        for key in "${!E2E_ARRAY[@]}"; do
            ITEM_ARRAY["$key"]="${E2E_ARRAY[$key]}"
        done
    elif [ ${ARGS} == "failed" ]; then
        typeset -A ITEM_ARRAY
        for key in "${!FAIL_ARRAY[@]}"; do
            ITEM_ARRAY["$key"]="${FAIL_ARRAY[$key]}"
        done
    fi

    for DURATION in ${DURATION_ARRAY[@]}
    do
        SUBTRACT=
        FORMAT=
        QUERY=
        for ITEM in ${ITEM_ARRAY[@]}
        do
            SUBTRACT=".subtract(.es(metric='max:${ITEM}', offset=-${DURATION}))"
                FORMAT="format_bps()"
                QUERY=${QUERY}".es(metric='max:${ITEM}')${SUBTRACT}.label(${ITEM}),"

            if [[ -z $FIELDS ]]; then
                FIELDS=${ITEM}
            else
                FIELDS=${FIELDS},${ITEM}
            fi
        done
        # echo "Query ${QUERY}"

        TITLE="${DURATION}_mixed_${ARGS}"
        VISUALIZATION_FILE=${VISUALIZATION_PATH}/${TITLE}.json

read -d '' output <<- EOF
{
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"query\\\":{\\\"query_string\\\":{\\\"query\\\":\\\"*\\\"}},\\\"filter\\\":[]}"
    },
    "version": 1,
    "description": "",
    "uiStateJSON": "{}",
    "visState": "{\\\"title\\\":\\\"${TITLE}\\\",\\\"type\\\":\\\"timelion\\\",\\\"params\\\":{\\\"expression\\\":\\\"${QUERY}\\\",\\\"interval\\\":\\\"${DURATION}\\\",\\\"fields\\\":\\\"${FIELDS}\\\"},\\\"aggs\\\":[],\\\"listeners\\\":{}}",
    "title": "${TITLE}"
}
EOF
        echo "Generated $VISUALIZATION_FILE"
        echo "${output}" > ${VISUALIZATION_FILE}

    done #For all Duration
}


generate_dashboard_granularity_mixed()
{
    ARGS=$1
    TYPE_ARRAY=()
    STAGE_ARRAY=()

    echo "Generating dashboard for mixed granularity ${ARGS}"

    if [ ${ARGS} == "lp" ]; then
        TYPE_ARRAY=${DISTR_TYPE_ARRAY[@]}
        MODULE=${MODULE_ARRAY[0]}
    else
        TYPE_ARRAY=${TYPE_ARRAY_CP[@]}
        MODULE=${MODULE_ARRAY[1]}
    fi


    PANEL=''
    PANELINDEX=1
    COL=7
    ROW=0

    for DURATION in ${DURATION_ARRAY[@]}
    do
        PANEL=''
        ROW=0
        COL=7
        PANELINDEX=1

        # Column should be swapped b/w 1 & 7
        if [ $ROW == 0 ]; then
            ROW=1
        else
            ROW=$(expr $ROW + 3) # Always increment it by 3
        fi

        for TYPE in ${TYPE_ARRAY[@]}
        do
            # Column should be swapped b/w 1 & 7
            if [ $COL == 7 ]; then
                COL=1
            elif [ $COL == 1 ]; then
                COL=7
            fi

            IDNAME="${DURATION}_mixed_${ARGS}_${TYPE}"
            CUR_OBJECT=$MODULE.${STAGE}_${TYPE}

LOC="{\\\\"\"id\\\\"\":\\\\"\"${IDNAME}\\\\"\", \\\\"\"row\\\\"\":${ROW}, \\\\"\"col\\\\"\":${COL}, \\\\"\"panelIndex\\\\"\":${PANELINDEX}, ${STATIC_CONST}}"

            if [[ -z $PANEL ]]; then
                PANEL="${LOC}"           # This is the first one
            else
                PANEL="${PANEL},${LOC}"  # This is NOT the first one
            fi

            PANELINDEX=$(expr $PANELINDEX + 1)
        done

        TITLE="${DURATION}_mixed_${ARGS}_dashboard"
        DASHBOARD_FILE=${DASHBOARD_PATH}/${TITLE}.json

read -d '' OUTPUT <<- EOF
{
    "title": "${TITLE}",
    "hits": 0,
    "description": "",
    "optionsJSON": "{\\\"darkTheme\\\":false}",
    "uiStateJSON": "{}",
    "version": 1,
    "timeRestore": false,
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"filter\\\":[{\\\"query\\\":{\\\"query_string\\\":{\\\"analyze_wildcard\\\":true,\\\"query\\\":\\\"*\\\"}}}],\\\"highlightAll\\\":true,\\\"version\\\":true}"
    },
    "panelsJSON": "[${PANEL}]"
}

EOF

        echo "$OUTPUT" > ${DASHBOARD_FILE}
        echo "Generated ${DASHBOARD_FILE}"
    done  # End of outer loop Granularity
}


generate_custom_dashboard_granularity_mixed()
{
    ARGS=$1
    echo "Generating custom dashboard for mixed granularity ${ARGS}"

    if [ ${ARGS} == "e2e" ]; then
        typeset -A ITEM_ARRAY
        for key in "${!E2E_ARRAY[@]}"; do
            ITEM_ARRAY["$key"]="${E2E_ARRAY[$key]}"
        done
    elif [ ${ARGS} == "failed" ]; then
        typeset -A ITEM_ARRAY
        for key in "${!FAIL_ARRAY[@]}"; do
            ITEM_ARRAY["$key"]="${FAIL_ARRAY[$key]}"
        done
    fi

    for DURATION in ${DURATION_ARRAY[@]}
    do
        PANEL=''
        ROW=0
        COL=1
        PANELINDEX=1
        IDNAME="${DURATION}_mixed_${ARGS}"


PANEL="{\\\\"\"id\\\\"\":\\\\"\"${IDNAME}\\\\"\", \\\\"\"row\\\\"\":${ROW}, \\\\"\"col\\\\"\":${COL}, \\\\"\"panelIndex\\\\"\":${PANELINDEX}, ${STATIC_CONST}}"

        TITLE="${DURATION}_mixed_${ARGS}_dashboard"
        DASHBOARD_FILE=${DASHBOARD_PATH}/${TITLE}.json
        FIELDS=
        FIELD=
        for ITEM in ${ITEM_ARRAY[@]}
        do
            FIELD="{\\\\"\"field\\\\"\":\\\\"\"${ITEM}\\\\"\"}"
            if [[ -z $FIELDS ]]; then
                FIELDS="${FIELD}"
            else
                FIELDS="${FIELDS},${FIELD}"
            fi
        done

read -d '' OUTPUT <<- EOF
{
    "title": "${TITLE}",
    "hits": 0,
    "description": "",
    "optionsJSON": "{\\\"darkTheme\\\":false}",
    "uiStateJSON": "{}",
    "version": 1,
    "timeRestore": false,
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"filter\\\":[{\\\"query\\\":{\\\"query_string\\\":{\\\"analyze_wildcard\\\":true,\\\"query\\\":\\\"*\\\"}}}],\\\"highlightAll\\\":true,\\\"version\\\":true}"
    },
    "panelsJSON": "[${PANEL},${FIELDS}]"
}

EOF

        echo "$OUTPUT" > ${DASHBOARD_FILE}
        echo "Generated ${DASHBOARD_FILE}"
    done  # End of outer loop Granularity
}


generate_custom_dashboard()
{
    ARGS=$1
    echo "Generating ${ARGS} dashboard"

    if [ ${ARGS} == "e2e" ]; then
        typeset -A ITEM_ARRAY
        for key in "${!E2E_ARRAY[@]}"; do
            ITEM_ARRAY["$key"]="${E2E_ARRAY[$key]}"
        done
    elif [ ${ARGS} == "failed" ]; then
        typeset -A ITEM_ARRAY
        for key in "${!FAIL_ARRAY[@]}"; do
            ITEM_ARRAY["$key"]="${FAIL_ARRAY[$key]}"
        done
    fi

    PANEL=''
    PANELINDEX=1
    COL=7
    ROW=0

    for DURATION in ${DURATION_ARRAY[@]}
    do
        PANEL=''
        ROW=0
        COL=7
        PANELINDEX=1

    for ITEM in ${!ITEM_ARRAY[@]}
    do
        # Column should be swapped b/w 1 & 7
        if [ $COL == 7 ]; then
            COL=1
            if [ $ROW == 0 ]; then
                ROW=1
            else
                ROW=$(expr $ROW + 3) # Always increment it by 3
            fi
        elif [ $COL == 1 ]; then
            COL=7
        fi

        IDNAME=${DURATION}_${ITEM}
        CUR_OBJECT=${ITEM_ARRAY[$ITEM]}

        LOC="{\\\\"\"id\\\\"\":\\\\"\"${IDNAME}\\\\"\", \\\\"\"row\\\\"\":${ROW}, \\\\"\"col\\\\"\":${COL}, \\\\"\"panelIndex\\\\"\":${PANELINDEX}, ${STATIC_CONST}, \\\\"\"field\\\\"\":\\\\"\"${CUR_OBJECT}\\\\"\"}"

        if [[ -z $PANEL ]]; then
            PANEL="${LOC}"           # This is the first one
        else
            PANEL="${PANEL},${LOC}"  # This is NOT the first one
        fi

        PANELINDEX=$(expr $PANELINDEX + 1)
        done

        TITLE="${DURATION}_${ARGS}_dashboard"
        DASHBOARD_FILE=${DASHBOARD_PATH}/${TITLE}.json

read -d '' OUTPUT <<- EOF
{
    "title": "${TITLE}",
    "hits": 0,
    "description": "",
    "optionsJSON": "{\\\"darkTheme\\\":false}",
    "uiStateJSON": "{}",
    "version": 1,
    "timeRestore": false,
    "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\\\"filter\\\":[{\\\"query\\\":{\\\"query_string\\\":{\\\"analyze_wildcard\\\":true,\\\"query\\\":\\\"*\\\"}}}],\\\"highlightAll\\\":true,\\\"version\\\":true}"
    },
    "panelsJSON": "[${PANEL}]"
}

EOF

    echo "$OUTPUT" > ${DASHBOARD_FILE}
    echo "Generated ${DASHBOARD_FILE}"

    done  # End of outer loop Granularity
}


generate_dashboard_granularity()
{
    echo "generating dashboard granularity"

    generate_dashboard_granularity_process "lp"
    generate_dashboard_granularity_process "cp"

    generate_dashboard_granularity_mixed "lp"
    generate_dashboard_granularity_mixed "cp"


    generate_custom_dashboard "e2e"
    generate_custom_dashboard "failed"

    generate_custom_dashboard_granularity_mixed "e2e"
    generate_custom_dashboard_granularity_mixed "failed"

    echo "completed generating dashboard granularity"
}


generate_visualization_granularity()
{
    echo "generating visualization granularity"

    generate_visualization_granularity_process "lp"
    generate_visualization_granularity_process "cp"

    generate_myri_ether_cplane_uplane_visualization_granularity_process "myricom"
    generate_myri_ether_cplane_uplane_visualization_granularity_process "ethernet"
    generate_myri_ether_cplane_uplane_visualization_granularity_process "uplane"
    generate_myri_ether_cplane_uplane_visualization_granularity_process "cplane"

    generate_visualization_granularity_mixed "lp"
    generate_visualization_granularity_mixed "cp"
    generate_custom_visualization_granularity_mixed "e2e"
    generate_custom_visualization_granularity_mixed "failed"

    echo "completed generating visualization granularity"
}


# This is currently unused.
load_index_pattern()
{
    for file in ${INDEX_PATTERN_PATH}/*.json
    do
    if [ ! -f $file ]; then
            continue;
    fi

    NAME=`awk '$1 == "\"title\":" {gsub(/[",]/, "", $2); print $2}' ${file}`
    echo "Loading index pattern ${NAME}:"

    ${CURL} -XPUT ${ELASTICSEARCH}/${KIBANA_INDEX}/index-pattern/${NAME}?pretty \
            -d @${file} || exit 1
    echo
    done
}


load_visualization()
{
    echo "Loading visualization"

    for file in ${VISUALIZATION_PATH}/*.json
    do
    NAME=`basename ${file} .json`
    echo "Loading visualization ${NAME}:"
    ${CURL} -XPUT ${ELASTICSEARCH}/${KIBANA_INDEX}/visualization/${NAME}?pretty \
            -d @${file} || exit 1
    echo
    done
}


load_dashboard()
{
    echo "Loading dashboards"

    for file in ${DASHBOARD_PATH}/*.json
    do
    NAME=`basename ${file} .json`
    echo "Loading dashboard ${NAME}:"
    ${CURL} -XPUT ${ELASTICSEARCH}/${KIBANA_INDEX}/dashboard/${NAME}?pretty \
            -d @${file} || exit 1
    echo
    done
}


# This is currently unused.
load_unused_search_data()
{

    if [ -f ${BEAT_CONFIG} ]; then
	for ln in `cat ${BEAT_CONFIG}`; do
	    BUILD_STRING="${BUILD_STRING}s/${ln}/g;"
	done
	SED_STRING=`echo ${BUILD_STRING} | sed 's/;$//'`
    fi
    # Failsafe
    if [ -z ${SED_STRING} ]; then
	SED_STRING="s/packetbeat-/packetbeat-/g;s/filebeat-/filebeat-/g;s/topbeat-/topbeat-/g;s/winlogonbeat-/winlogonbeat-/g"
    fi

    DIR=dashboards

    echo "Loading dashboards to ${ELASTICSEARCH} in ${KIBANA_INDEX}"

    ## Workaround for: https://github.com/elastic/beats-dashboards/issues/94
    #${CURL} -XPUT "${ELASTICSEARCH}/${KIBANA_INDEX}"
    #${CURL} -XPUT "${ELASTICSEARCH}/${KIBANA_INDEX}/_mapping/search" -d'{"search": {"properties": {"hits": {"type": "integer"}, "version": {"type": "integer"}}}}'

    TMP_SED_FILE="${DIR}/search/tmp_search.json"
    for file in ${DIR}/search/*.json
    do
	if [ ! -f $file ]; then
            continue;
	fi

	NAME=`basename ${file} .json`
	echo "Loading search ${NAME}:"
	sed ${SED_STRING} ${file} > ${TMP_SED_FILE}
	${CURL} -XPUT ${ELASTICSEARCH}/${KIBANA_INDEX}/search/${NAME}?pretty \
            -d @${TMP_SED_FILE} || exit 1
	echo
    done

    if [ -e ${TMP_SED_FILE} ]; then
	rm ${TMP_SED_FILE}
    fi

}


while [ "$1" != "" ];
do
    case $1 in
	-l | -url )
            ELASTICSEARCH=$2
            if [ "$ELASTICSEARCH" = "" ]; then
		echo "Error: Missing Elasticsearch URL"
		print_usage
		exit 1
            fi
            ;;

	-u | -user )
            USER=$2
            if [ "$USER" = "" ]; then
		echo "Error: Missing username"
		print_usage
		exit 1
            fi
            CURL="${CURL} --user ${USER}"
            ;;

	-i | -index )
            KIBANA_INDEX=$2
            if [ "$KIBANA_INDEX" = "" ]; then
		echo "Error: Missing Kibana index pattern"
		print_usage
		exit 1
            fi
            ;;

	-h | -help )
            print_usage
            exit 0
            ;;

	-gv | -gen_visualization )
	    generate_visualization_granularity
	    exit 0
	    ;;

	-gd | -gen_dashboard )
	    generate_dashboard_granularity
	    exit 0
	    ;;

	*)
            echo "Error: Unknown option $2"
            print_usage
            exit 1
            ;;

    esac
    shift 2
done

# Initially run these two functions for granularity
generate_visualization_granularity
generate_dashboard_granularity

# Loading the data
load_visualization
load_dashboard
