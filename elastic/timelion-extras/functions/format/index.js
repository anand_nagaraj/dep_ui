var alter = require('../../../../src/core_plugins/timelion/server/lib/alter.js');
var Chainable = require('../../../../src/core_plugins/timelion/server/lib/classes/chainable');
var _ = require('lodash');
var Promise = require('bluebird');

function compareNumbers(a, b) {
  return a - b;
}

module.exports = new Chainable('format', {
  args: [
    {
      name: 'inputSeries',
      types: ['seriesList']
    }
  ],
  help: 'Format number to Bytes/Kb/Mb/Gb/Tb/Pb/Eb/Zb/Yb.',

  fn: function formatFn(args) {
    var seriesList = args.byName.inputSeries;

    var values = Array();
    for (var i = 0; i < seriesList.list[0].data.length; ++i) {
        var bytes = seriesList.list[0].data[i][1];
        if (bytes > 0) {
            values.push(bytes);
        }
    }

    var lowest_value = values.sort(compareNumbers)[0];
    if (lowest_value) {
        var index = parseInt(Math.floor(Math.log(lowest_value) / Math.log(1024)), 10);
        if (index) {
            var converted_value = ((lowest_value) / (1024 ** index)).toFixed(1)
            if (converted_value < 10) {
                index = index - 1;
            }
        }

        const sizes = ['Bytes', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb']
        var label = sizes[index];
        seriesList.list[0].label = seriesList.list[0].label.concat("_in_").concat(label);

        var data =  Array();
        for (var i = 0; i < seriesList.list[0].data.length; ++i) {
            var bytes = seriesList.list[0].data[i][1];
            if (bytes > 0) {
                var converted_bytes = (bytes / (1024 ** index)).toFixed(1);
                data.push([seriesList.list[0].data[i][0], Math.ceil(converted_bytes)])
            } else {
                data.push([seriesList.list[0].data[i][0], null])
            }
        }
        seriesList.list[0].data = data;
    }

    return seriesList;
  }
});
