#!/bin/bash

#set -x

anyothertemplate=`curl -XGET http://localhost:9200/_template/logstash?pretty`
if [ "${anyothertemplate}" != "{ }" ]; then
    echo "Removing all previous template registered to logstash indices"
    curl -XDELETE http://localhost:9200/_template/logstash?pretty
fi

ourtemplate=`curl -XGET http://localhost:9200/_template/logstash_per_index?pretty`
if [ "${ourtemplate}" != "{ }" ]; then
    echo "Removing Our template"
    curl -XDELETE http://localhost:9200/_template/logstash_per_index?pretty
fi

echo "Applying new template"
#Push a new template which elasticsearch will have it
curl -XPUT http://localhost:9200/_template/logstash_per_index?pretty -d @templates/dep.template.json

echo "Replaying back the new template"
#GET a new index and display it on screen.. (only for debugging purpose)
curl -XGET http://localhost:9200/_template/logstash_per_index?pretty

echo "Removing all previous stored data"
# Remove all data in database if there is one, this will remove data starting from logstash-2017.08.31
curl -XDELETE http://localhost:9200/logstash*?pretty

