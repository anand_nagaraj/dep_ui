#!/bin/bash

#set -x

ELASTICSEARCHURL="localhost:9200"
DIR="/tmp"
IGNORE=".kibana"
DB_DUMPER="./elasticdump/bin/elasticdump"

COMPRESS=0

EXPORT=0
IMPORT=0

print_usage()
{
    echo "
Usage:
  $(basename "$0") -url ${ELASTICSEARCHURL} -export -import -compress -path ${DIR}

Options:
   -h | --help
       Print the help menu
   -l | -url
       Elasticseacrh URL. By default is ${ELASTICSEARCHURL}.
   -e | -export
       Export the database to a file from ES
   -i | -import
       Import the database from file to ElasticSearch
   -c | -compress
       Optional compress tool, to be used only while exporting
   -p | -path
       Specifies the path where the database will be dumped while export
       And also the same path for reading json files to restoring the database
" >&2
}

export_database()
{
    # Get list of indices
    INDICES=`curl -XGET http://localhost:9200/_cat/indices | grep -v ${IGNORE} | cut -d ' ' -f 3 | sort`

    ARRAY_INDICES=($INDICES)

    for INDEX in "${ARRAY_INDICES[@]}"
    do
	# Only for 'logstash' indices
	if [[ ${INDEX} == logstash* ]];
	then
	    OUTPUTFILE="${DIR}/${INDEX}.json"

	    if [ -e ${OUTPUTFILE} ]; then
		rm -f ${OUTPUTFILE}
	    fi

	    # Dump index to JSON
	    ${DB_DUMPER} --input="http://${ELASTICSEARCHURL}/$INDEX" --output=${OUTPUTFILE}

	    if [ ${COMPRESS} == 1 ]; then
		# Compress dump
		zip -r ${OUTPUTFILE}.zip ${OUTPUTFILE}

		# Delete JSON file
		rm ${OUTPUTFILE}
	    fi

	    # Delete index (!)
	    # Uncomment only if you know what you're doing
	    # curl -XDELETE ${ELASTICSEARCHURL}/$INDEX
	fi
    done
}

import_database()
{
    if [ ! -d ${DIR} ]; then
	echo "There is no directory here."
	exit 2
    fi

    # Unzip all the files if they were compressed
    for FILE in ${DIR}/*
    do
	FILE=$(basename $FILE)
	echo $FILE

	OUTPUTFILE="$(basename ${DIR}/${FILE} .zip)"
	if [ -e ${DIR}/${OUTPUTFILE} ]; then
	    rm -f ${DIR}/${OUTPUTFILE}
	fi

	if [[ ${FILE} =~ \.zip$ ]]; then
	    unzip -j "${DIR}/${FILE}" -d ${DIR}/
	fi
    done

    # At this point, all the files are uncompressed and in json format
    for FILE in ${DIR}/*
    do
	# Don't work on those files, which are not json files.
	if [[ ${FILE} =~ \.json$ ]]; then
	    INDEX=$(basename ${FILE} .json)
	    ${DB_DUMPER} --input=${FILE} --output="http://${ELASTICSEARCHURL}/$INDEX"
	fi
    done
}


while [ "$1" != "" ];
do
    case $1 in
	-l | -url )
            ELASTICSEARCHURL=$2
            if [ "$ELASTICSEARCHURL" = "" ]; then
		echo "Error: Missing Elasticsearch URL"
		print_usage
		exit 1
            fi
            ;;

	-e | -export )
	    if [ ${IMPORT} == 1 ]; then
		echo "Export and Import are mutually exclusive"
		exit 1
	    fi
	    EXPORT=1
	    ;;

	-i | -import )
	    if [ ${EXPORT} == 1 ]; then
		echo "Export and Import are mutually exclusive"
		exit 1
	    fi
	    IMPORT=1
	    ;;

	-c | -compress )
	    COMPRESS=1
	    ;;

	-p | -path )
	    if [ ! -d $2 ]; then
		echo "Directory was not exisiting, hence creating one";
		mkdir -p "$2"
	    fi
	    DIR=$2
	    shift 1
	    ;;
	*)
	    echo "Error: Unknown option $2"
            print_usage
            exit 1
            ;;

    esac
    shift 1
done


if [ ${EXPORT} == 1 ]; then
    export_database
elif [ ${IMPORT} == 1 ]; then
    import_database
else
    print_usage
    exit 1
fi
