import requests
import json

'''
Gathers input from the user
duration --> gets the granularity details
fields --> Specifies which objects is it of interest to us
start_time --> start time of the range
end_time --> end time of the range
'''
class InputData(object):
    def __init__(self, dur, fd, stime, etime):
        self.duration = dur
        self.fields = fd
        self.start_time = stime
        self.end_time = etime


class RestQueries(object):
    def __init__(self):
        '''Some initilizers here if any'''
        self.query_obj = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "analyze_wildcard": 'true',
                                "query": "*"
                            }
                        },
                        {
                            "range": {
                                "@timestamp": {
                                    "format": "epoch_millis"
                                }
                            }
                        }
                    ],
                    "must_not": []
                }
            },
            "aggs": {
                "2": {
                    "date_histogram": {
                        "field": "@timestamp",
                        "time_zone": "Australia/Sydney",
                        "min_doc_count": 1
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "version": 'true'
        }



    def request(self, uri):
        response = requests.get(uri)
        return response


    '''Need this function for properly query'''
    def search(self, uri):
        """Simple Elasticsearch Query"""

        query = json.dumps(self.query_obj)
        response = requests.get(uri, data=query)
        results = json.loads(response.text)
        print(results)

        return results  # returning json format output


    def update_query_fields(self, input_data):
        fields = input_data.fields
        if len(fields) == 0:
            return 1;

        all_agg = {}
        range = {}
        ts = {}
        must = {}
        for field in fields:
            all_agg[field]= {"max":{"field" : field}}

        if len(all_agg) > 0:
            self.query_obj['aggs']['2']['aggs'] = all_agg

        ts['format'] = 'epoch_millis'
        ts['gte'] = input_data.start_time
        ts['lte'] = input_data.end_time
        range['@timestamp'] = ts
        must['range'] = range

        # Filling other details from input_data
        self.query_obj['aggs']['2']['date_histogram']['interval'] = input_data.duration
        self.query_obj['query']['bool']['must'][1] = must

        #print(self.query_obj)