from rest_queries import RestQueries, InputData
from CSVWriter import CSVWriter

def start_query(pattern, rest_query, fields):
    uri_indices = 'http://localhost:9200/_cat/indices/' + pattern + '?v&h=i'

    response = rest_query.request(uri_indices)

    print(response.text)
    indices = response.text.split()

    writer = CSVWriter()

    for index in indices:
        if index == 'i':
            continue #Ignore this as this is just a field entry.

        uri_search = 'http://localhost:9200/' + index + '/_search'

        results = rest_query.search(uri_search)
        writer.print_results(results, index, fields)


if __name__ == "__main__":
    '''Start of the script'''
    fields = ['dep_lp.distr.incoming_bytes',
              'dep_lp.distr.failed_bytes',
              'dep_lp.distr.passed_bytes',
              'dep_lp.distr.gtpu_bytes'
             ]
    #input_data = InputData('1m', fields, 1502990400000, 1502994599999)
    input_data = InputData('10m', fields, 1502989200000, 1502996399999)
    rest_query = RestQueries() # RestQueries object

    rest_query.update_query_fields(input_data)

    start_query('logstash-*', rest_query, fields)
