class CSVWriter(object):
    def __init__(self, fname='/tmp/objectdump.csv'):
        self.fname = fname
        pass

    '''This module will definitely will phase out.'''
    def print_results(self, results, index, fields):
        """Print results nicely: doc_id) content
        """

        total = results['hits']['total']
        if total == 0:
            return

        buckets = [bucket for bucket in results['aggregations']['2']['buckets']]

        print('Data was available in index %s' % index)

        prev_val = {}
        cur_val = {}
        delta = {}

        for row in buckets:
            key = row['key']
            key_as_string = row['key_as_string']

            for field in fields:
                cur_val[field] = row[field]['value']

                if cur_val[field] is not None:
                    delta[field] = 0 # cur_val[field] # Either 0 or first value
                    if field in prev_val.keys():
                        delta[field] = cur_val[field] - prev_val[field]

                prev_val[field] = cur_val[field]

            #NOTE: WE SHOULD BE IGNORING THE FIRST VALUE, AS IT MIGHT BE CONTAINING either the first value or 0.
            print('%s : %s : %s' % (key, key_as_string, delta))

            # Here is where we need to transfer it to csv file.
            # Or better way is to collect as one single object in a list or dict and dump it at once, which is better.
