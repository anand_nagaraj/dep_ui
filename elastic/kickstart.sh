#!/bin/sh

VER=5.5.1
KIBANATAR=kibana-${VER}-linux-x86_64.tar.gz
LOGSTASHTAR=logstash-${VER}.tar.gz
FILEBEATTAR=filebeat-${VER}-linux-x86_64.tar.gz
ELASTICSRCHTAR=elasticsearch-${VER}.tar.gz
ELASTICDUMPTAR=elasticdump.tar.gz
METRICBEATTAR=metricbeat-${VER}-linux-x86_64.tar.gz

CONFIGDIR=configuration
KIBANADIR=kibana-${VER}-linux-x86_64
ELASTICSRCHDIR=elasticsearch-${VER}
LOGSTASHDIR=logstash-${VER}
FILEBEATDIR=filebeat-${VER}-linux-x86_64
METRICBEATDIR=metricbeat-${VER}-linux-x86_64
ELASTICDUMPDIR=elasticdump
TIMELIONEXTRASDIR=timelion-extras
DEPKIBANACHANGES=dep-kibana-changes
KIBANAPUBLICDIR=$KIBANADIR/src/core_plugins/kibana/public
DEPMODULEDIR=$KIBANAPUBLICDIR/dep_modules/
KIBANADASHBOARD=$KIBANAPUBLICDIR/dashboard
KIBANAVISBOARD=$KIBANAPUBLICDIR/visualize

#set -x

if [ -f $KIBANATAR ]; then
    if [ ! -d $KIBANADIR ]; then
        tar -xzvf $KIBANATAR
    fi
fi

if [ -f $LOGSTASHTAR ]; then
    if [ ! -d $LOGSTASHDIR ]; then
        tar -xzvf $LOGSTASHTAR
    fi
fi

if [ -f $FILEBEATTAR ]; then
    if [ ! -d $FILEBEATDIR ]; then
        tar -xzvf $FILEBEATTAR
    fi
fi

if [ -f $METRICBEATTAR ]; then
    if [ ! -d $METRICBEATDIR ]; then
        tar -xzvf $METRICBEATTAR
    fi
fi

if [ -f $ELASTICSRCHTAR ]; then
    if [ ! -d $ELASTICSRCHDIR ]; then
        tar -xzvf $ELASTICSRCHTAR
    fi
fi

if [ -f$ELASTICDUMPTAR ]; then
    if [ ! -d $ELASTICDUMPDIR ]; then
        tar -xzvf $ELASTICDUMPTAR
    fi
fi

echo "copying the configuration file to corresponding locations"
# copy all configuration files
cp $CONFIGDIR/kibana.yml $KIBANADIR/config/
cp $CONFIGDIR/elasticsearch.yml $ELASTICSRCHDIR/config/

# copy timelion extra functions
cp -rf $TIMELIONEXTRASDIR $KIBANADIR/plugins/

if [ ! -d $DEPMODULEDIR ]; then
    mkdir $DEPMODULEDIR
fi

if [ -d $DEPMODULEDIR ]; then
    echo "Copying kibana related files to $DEPMODULEDIR"
    cp -f $DEPKIBANACHANGES/csv_writer.js $DEPMODULEDIR/
    cp -f $DEPKIBANACHANGES/customize_dep_action.js $DEPMODULEDIR/
    cp -f $DEPKIBANACHANGES/rest_queries.js $DEPMODULEDIR/
fi

echo "Copying kibana related dashboard and visualization"
cp -f $DEPKIBANACHANGES/dashboard.js $KIBANADASHBOARD/
cp -f $DEPKIBANACHANGES/editor.js $KIBANAVISBOARD/editor/
cp -f $DEPKIBANACHANGES/top_nav/*.js $KIBANADASHBOARD/top_nav/

